#-------------------------------------------------
#
# Project created by QtCreator 2015-04-26T14:01:44
#
#-------------------------------------------------

QT       += core gui xml

# With C++11 support
macx {
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.7
    QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
    QMAKE_CFLAGS += -mmacosx-version-min=10.7
    QMAKE_MAC_SDK = macosx10.9
}

macx {
    QMAKE_CXXFLAGS += -std=c++11 -stdlib=libc++
    QMAKE_CFLAGS += -std=c++11 -stdlib=libc++
}
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = logAnalyserGui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    statsmanager.cpp

HEADERS  += mainwindow.h \
    statsmanager.h

FORMS    += mainwindow.ui
