#ifndef STATSMANAGER
#define STATSMANAGER

#include <string>
#include <fstream>
#include <iostream>
#include <QThread>

#include <map>

using namespace std;
void callStats(string);

class StatsManager{
private:
    int creates,adds,movetos,moveafters,movefroms,removes,readings,sets,modelings,comments,lines;
    unsigned long totaltime;
    multimap<unsigned long,string> myMap;
    string line;

public:
    void reset();
    StatsManager();
    bool showStats(string fileName, ofstream& outfile);
    bool showXMIStats(string fileName);

};

#endif // STATSMANAGER

