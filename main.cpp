#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    fstream myfile;
      myfile.open ("example.txt", fstream::in | fstream::out | fstream::trunc);
      myfile << "Writing this to a file.\n";
      myfile.close();
    return a.exec();
}
